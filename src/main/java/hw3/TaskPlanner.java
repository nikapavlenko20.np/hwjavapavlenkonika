package hw3;

import java.util.Optional;
import java.util.Scanner;

public class TaskPlanner {

    static Scanner scanner = new Scanner(System.in);

    public static Optional<String> checkString(String s) {
            String[] dayWeek = {"Sunday", "Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
            for (String a: dayWeek){
                if (a.equalsIgnoreCase(s) || a.equalsIgnoreCase(s.trim()) || s.equalsIgnoreCase("exit")){
                    String str = s.toLowerCase().replace(s.split("")[0], s.split("")[0].toUpperCase()).trim();

                    return Optional.of(s.equalsIgnoreCase("exit") ? s.toLowerCase() : str);
                }
            }
            return Optional.empty();
    }

    public static String expectedString(){
        String day = scanner.next();
        Optional<String> uerDay = checkString(day);

        if (!uerDay.isPresent()){
            System.out.println("print day. try again!");
            return printString();
        }
        return uerDay.get();
    }


    public static String printString(){
        System.out.println("Please, input the day of the week:");

        String day = expectedString();

        return day;
    }


    public static void startPlanner(String[][] scedule) {
        boolean flag = true;
        do {
            String day = printString();

            switch (day) {
                case "Sunday":
                    System.out.printf("Your tasks for Sunday: %s \n", scedule[0][1]);
                    break;
                case "Monday":
                    System.out.printf("Your tasks for Monday: %s \n", scedule[1][1]);
                    break;
                case "Tuesday":
                    System.out.printf("Your tasks for Tuesday: %s \n", scedule[2][1]);
                    break;
                case "Wednesday":
                    System.out.printf("Your tasks for Wednesday: %s \n", scedule[3][1]);
                    break;
                case "Thursday":
                    System.out.printf("Your tasks for Thursday: %s \n",scedule[4][1]);
                    break;
                case "Friday":
                    System.out.printf("Your tasks for Friday: %s \n",scedule[5][1]);
                    break;
                case "Saturday":
                    System.out.printf("Your tasks for Saturday: %s \n",scedule[6][1]);
                    break;
                case "exit":
                    flag = false;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    startPlanner(scedule);
            }
        } while (flag);

    }




    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to shopping";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to fitness";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to courses";
        scedule[5][0] = "Friday";
        scedule[5][1] = "watch a film";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "cooking";

        startPlanner(scedule);

    }
}
