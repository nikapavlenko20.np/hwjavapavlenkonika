package hw4;

import java.util.Set;

public class Fish extends Pet {
    private Species species = Species.Fish;


    public Fish(String nickname){
        super(nickname);
        setSpecies(species);
    }

    public Fish( String nickname, int age, int trickLevel,  Set<String> habits){
        super (nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    public Fish(){
        setSpecies(species);
    }


    @Override
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", getNickname());
    }
}
