package hw2;

import java.util.Optional;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AreaShooting {

    static Scanner scanner = new Scanner(System.in);

    public static int random(int min, int max){
        int delta = max-min;
        return (int) (Math.random()*(delta+1)+min);
    }

    public static int toInt(String a) {
        return Integer.parseInt(a);
    }

    public static Optional<Integer> isInt(String a) {
            Pattern pattern = Pattern.compile("[1-9]");
            Matcher matcher  = pattern.matcher(a);
            if (matcher.matches()){
                toInt(a);
                return Optional.of(toInt(a));
            }
            return  Optional.empty();
    }

    public static int expectInt(String coordinate){
        String value = expctString();

        Optional<Integer> userNum = isInt(value);
        if (!userNum.isPresent() || userNum.get()>=6) {
            System.out.println("print number by 1 to 5. try again!");
            return printInt(coordinate);
        }
        return userNum.get();
    }

    public static String expctString() {
        return scanner.next();
    }

    public static int printInt(String coordinate){
        System.out.printf("Print %s", coordinate);
        int userNum = expectInt(coordinate);
        return userNum;
    }

    public static String[][] createFieldGame(String[][] field){
        for (int i =0; i<field.length; i++){
            for(int j=0;j<field.length; j++){
                field[i][j] = "-";
                if (i==0 ){
                    field[i][j] = Integer.toString(j);
                }
                if ( j ==0){
                    field[i][j] = Integer.toString(i);
                }
            }
        }

        return field;
    }

    public static void printField(String[][] field) {
        StringJoiner joiner = new StringJoiner("");

        for (int i =0; i<field.length; i++){
            for (int j=0; j<field.length; j++){
                joiner.add (String.format("%2s |", field[i][j]));
            }
            joiner.add("\n");
        }

        System.out.println(joiner);
    }

    public static int[] createSystemCoordinate(int fieldSize) {
        int[] ints = new int[2];
        System.out.println();
        for (int i=0;i<ints.length; i++){
            ints[i] = random(1,fieldSize-1);
        }
         return ints;
    }

    public static void game(String[][] field, int fieldSize) {
        boolean isWine= false;

        int systemCoordinateX = createSystemCoordinate(fieldSize)[0];
        int systemCoordinateY = createSystemCoordinate(fieldSize)[1];

        do {
            printField(field);
            int xUser = printInt("x");

            int yUser = printInt("y");

            if (systemCoordinateX!=xUser || systemCoordinateY!=yUser){
                field[yUser][xUser] = "*";
            } else {
                isWine= true;
                System.out.println("You have won!");
            }
        } while (!isWine);
    }

    public static void startGame(){
        int fieldSize = 6;
        String[][] field = new String[fieldSize][fieldSize];

        System.out.println("All set. Get ready to rumble!");
        createFieldGame(field);
        game(field, fieldSize);
    }

    public static void main(String[] args) {
        startGame();
    }
}
