package hw4;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;

public abstract class Human implements java.io.Serializable{
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Human mother;
    private Human father;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    static private long bornBirthdayNow(){
        String db = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate l = LocalDate.parse(db, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return LocalDateTime.of(l.getYear(), l.getMonth(), l.getDayOfMonth(), 0, 0,0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    static private long getBirthday(String birthDate){
        LocalDate l = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return LocalDateTime.of(l.getYear(), l.getMonth(), l.getDayOfMonth(), 0, 0,0).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public Human(String name, String surname, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = bornBirthdayNow();
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name,String surname){
        this.name = name;
        this.surname = surname;
        this.birthDate = bornBirthdayNow();
    }


    public Human(String name,String surname, String birthDate, int iq){
        this.name = name;
        this.surname = surname;
        this.birthDate = getBirthday(birthDate);
        this.iq = iq;
    }

    public Human(String name,String surname, Family family){
        this.name = name;
        this.surname = surname;
        this.birthDate = bornBirthdayNow();
        this.mother = family.getMother();
        this.father = family.getFather();
        this.family = family;
    }

    public Human(String name,String surname,int iq, Map<DayOfWeek, String> schedule, Family family){
        this.name = name;
        this.surname = surname;
        this.birthDate = bornBirthdayNow();
        this.schedule = schedule;
        this.mother = family.getMother();
        this.father = family.getFather();
        this.family = family;
        this.iq = iq;

    }

    public Human(){}

    public String describeAge(){
        LocalDate ld = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(ld, LocalDate.now());
        return "Years " + period.getYears() + " months " + period.getMonths() + " Days " + period.getDays();
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getSurname(){
        return surname;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }

    public LocalDate getBirthDate(){
        return Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public void setBirthDate(String birthDate){
        this.birthDate = getBirthday(birthDate) ;
    }

    public Period getYear(){
        LocalDate ld = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        return Period.between(ld, LocalDate.now());
    }

    public int getOld(){
        LocalDate ld = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        return Period.between(ld, LocalDate.now()).getYears();
    }



    public int getIq(){
        return iq;
    }
    public void setIq(int iq){
        this.iq = iq ;
    }

    public Human getMother(){
        return mother;
    }
    public void setMother(Human mother){
        this.mother = mother;
    }

    public Human getFather(){
        return father;
    }
    public void setFather(Human father){
        this.father = father;
    }

    public Map<DayOfWeek, String> getSchedule() {return schedule;}
    public void setSchedule(DayOfWeek day ,String schedule ){
        this.schedule.put(day, schedule);
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }


    public void greetPet(){
        family.getPet().forEach(x -> System.out.printf("Привет, %s\n", x.getNickname()));
    }

    public void feedPet(){
        System.out.println("накормить");
    }


    public void describePet(){
        family.getPet().forEach(x -> {
            String patTrickLevel = (x.getTrickLevel() > 50) ? " он очень хитрый" : "почти не хитрый";
            System.out.printf("У меня есть %s, ему %d лет, он %s\n", x.getSpecies(), x.getAge(), patTrickLevel);
        });
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(mother, human.mother) &&
                Objects.equals(father, human.father);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, birthDate, iq, mother, father, family);
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate() +
                ", iq=" + iq +
                ", schedule=" + (schedule == null ? "" : schedule) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.printf("Human{name= %s , surname= %s, year= $d, iq= %d, schedule= %s }", name, surname, Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate(), iq,(schedule == null ? "" : schedule));
    }
}
