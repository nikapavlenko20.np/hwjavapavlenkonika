package hw4;
import java.util.Optional;
import java.util.Scanner;


public class Application {

    static Scanner scanner = new Scanner(System.in);
    static FamilyController controller = new FamilyController();



    public static String expctString() {
        return scanner.next();
    }

    static public String printMenu(){
        return String.format(
                "1. Отобразить весь список семей \n " +
                        "2. Отобразить список семей, где количество людей больше заданного \n " +
                        "3. Отобразить список семей, где количество людей меньше заданного \n " +
                        "4. Подсчитать количество семей, где количество членов равно \n " +
                        "5. Создать новую семью \n 6. Удалить семью по индексу семьи в общем списке \n " +
                        "7. Редактировать семью по индексу семьи в общем списке \n " +
                        "8. Сохранить все семьи/ прочитать все семьи \n " +
                        "9. Удалить всех детей старше возраста \n"
                );
    }

    public static int toInt(String a) {
        return Integer.parseInt(a);
    }

    public static Optional<Integer> isInt(String a) {
        try{
            toInt(a);
            return Optional.of(toInt(a));
        } catch (Exception x ){
            return Optional.empty();
        }
    }

    public static int expectInt(String str, int maxVal){
        String value = expctString();

        Optional<Integer> userNum = isInt(value);
        if (!userNum.isPresent() || userNum.get()>=maxVal ) {
            System.out.println("try again!");
            return printInt(str,maxVal);
        }
        return userNum.get();
    }

    public static int printInt(String str, int maxVal){
        System.out.printf(str);
        int userNum = expectInt(str, maxVal);
        return userNum;
    }

    static private void menuCase(){
        Integer numberMenu = expectInt("", 9);
        switch (numberMenu){
            case 1:
                FamilyService.getFamily().getAllFamilies().forEach(x -> System.out.println(x.prettyFormat()));
                return;
            case 2:
                controller.getFamiliesBiggerThan(printInt("запросить интересующее число", 1000));
                return;
            case 3:
                controller.getFamiliesLessThan(printInt("запросить интересующее число" ,1000));
                return;
            case 4:
                System.out.println(controller.getFamiliesInCount(printInt("запросить интересующее число", 1000)));
                return;
            case 5:
                createNewFamily();
                return;
            case 6:
                deleteFamily();
                return;
            case 7:
                updateFamily();
                return;
            case 8:
                controller.loadData();
                break;
            case 9:
                controller.deleteAllChildrenOlderThen(printInt("интересующий возраст", 1000));
        }
    }

    static void deleteFamily(){
        Integer c = printInt("запросить интересующее число",controller.count());
        FamilyService.getFamily().deleteFamilyByIndex(c);
    }

    static void updateFamily(){
        Integer s = expectInt("1. Родить ребенка \n2. Усыновить ребенка\n3. Вернуться в главное меню \n", 3);
        switch (s){
            case 1:
                bornChild();
                return;
            case 2:
                adoptChild();
                return;
            case 3:
                return;
        }
    }

    static void bornChild(){
        Integer numberFamily1 = printInt("порядковый номер семьи", controller.count()-1);
        System.out.println(" Выберете имя ребенка. для девочки - Nika, для мальчика - Alex");
        String nameChild = expctString();
        controller.bornChild(numberFamily1, nameChild);
    }

    static void adoptChild(){
        Integer numberFamily2 = printInt("порядковый номер семьи", controller.count()-1);
        System.out.println("Выберите пол ребенка, для мальчика M, для девочки W");
        String genderChildStr = expctString();
        Human child = new Man();
        if (genderChildStr=="M")  child = createHuman("ребенка", new Man());
        if (genderChildStr=="W")  child = createHuman("ребенка", new Woman());
        controller.adoptChild(numberFamily2, child);
    }

    static Human createHuman(String s, Human cls){
        System.out.printf("имя %s например, Анна", s);
        String name = expctString();
        System.out.printf("фамилию %s, например, Петренко", s);
        String surname = expctString();
        String yearStr = String.format("год рождения %sб напрвимен 2000", s);
        Integer yearBirth = printInt(yearStr, 2021);
        String monthStr = String.format("месяц рождения %s, от 1-12", s);
        Integer monthBirth = printInt(monthStr, 12);
        String dayStr = String.format("день рождения %s, 1-13", s);
        Integer dayBirth = printInt(dayStr, 31);
        String iqStr = String.format("iq %sб числом", s);
        Integer iq = printInt(iqStr, 200);
        if (cls.getClass() == Woman.class) return new Woman(name, surname, doBirthFormat(yearBirth, monthBirth, dayBirth), iq);
        if (cls.getClass() == Man.class) return new Man(name, surname, doBirthFormat(yearBirth, monthBirth, dayBirth), iq);
        return new Man(name, surname, doBirthFormat(yearBirth, monthBirth, dayBirth), iq);
    }

    static void createNewFamily(){
        Human mother = createHuman("матери", new Woman());
        Human father = createHuman("отца ", new Man());

        Family family = new Family(mother, father);

        FamilyService.getFamily().saveFamily(family);
    }

    static String doBirthFormat(int year, int month, int day){
        return String.format("%s/%s/%s", day, month, year);
    }

    public static void main(String[] args) {
        boolean flag = true;
        do {
            System.out.println(printMenu());
            menuCase();
        } while (flag);
    }
}
