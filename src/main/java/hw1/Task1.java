package hw1;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {

    static Scanner scanner = new Scanner(System.in);


    public static int randomNum (int min, int max) {
        int delta = max-min;
        return (int) (Math.random()*(delta+1) + min);
    }

    public static String printName(){
        System.out.println("Print your name");
        return scanner.next();
    }

    public static int printNumber(){
        System.out.println("Print your number");
        return scanner.nextInt();
    }


    public static void startGame(){
        System.out.println("Let the game begin!");
        String name = printName();
        int randomNum =  randomNum(0,100);
        game(name, randomNum);

    }


    public static void game(String name, int randomNum){
        boolean isFinish= false;
        do {
            int numberUser = printNumber();



            if (numberUser<randomNum) System.out.println("Your number is too small. Please, try again.");
            if (numberUser>randomNum) System.out.println("Your number is too big. Please, try again.");
            if (numberUser==randomNum) {
                System.out.printf("Congratulations, %s !\n", name);
                isFinish= true;
            }
        } while (!isFinish);
    }

    public static int[] sortArrys(int[] arr){
        int a;

        for (int i=0;i<arr.length; i++) {
            for (int j = 0; j<(arr.length-i); j++){
                if (arr[j]<arr[j+1]){
                    a = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1]= a;
                }
            }
        }

        return arr;
    }

    public static void main(String[] args) {
        startGame();
    }
}
