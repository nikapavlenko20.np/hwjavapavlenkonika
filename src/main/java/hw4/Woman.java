package hw4;

import java.util.Map;

public final class Woman extends Human {

    public Woman(String name, String surname, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname,iq, schedule);
    }

    public Woman(String name,String surname){
        super(name, surname);
    }

    public Woman(String name,String surname, String birthDate, int iq){
        super(name, surname, birthDate, iq);
    }

    public Woman(String name,String surname, Family family){
        super(name, surname, family);
    }

    public Woman(String name,String surname, int iq, Map<DayOfWeek, String> schedule, Family family){
        super(name, surname,iq, schedule, family);
    }

    public Woman(){}

    public void greetPet(){
        getFamily().getPet().forEach(x -> System.out.printf("Привет, %s\n", x.getNickname()));
    }

    public void makeup(){
        System.out.println("Моя прекрасная машина");
    }

}
