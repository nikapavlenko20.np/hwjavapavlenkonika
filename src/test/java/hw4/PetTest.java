package hw4;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {

    @Test
    public void testToString1(){
        String expect = "Dog{nickname='mary', age=0, trickLevel=0, habits=}";
        Pet dog = new Dog( "mary");
        String real = dog.toString();
        assertEquals(expect,real);
    }

    @Test
    public void testToString2(){
        String expect = "RoboCat{nickname='mary', age=1, trickLevel=3, habits=[eat]}";
        Set<String> habits = new HashSet<String>(){{
            add("eat");
        }};
        Pet dog = new RoboCat( "mary",1,3, habits);
        String real = dog.toString();
        assertEquals(expect,real);
    }
}
