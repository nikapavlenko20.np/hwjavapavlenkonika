package hw4;

import java.util.Arrays;
import java.util.Set;

public abstract class Pet {
   private  Species species = Species.UNKNOWN;
   private String nickname;
   private int age;
   private int trickLevel;
   private Set<String> habits;


    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname){
        this.nickname = nickname;
        this.trickLevel = 0;
        this.age = 0;
    }

    public Pet(){
    }


    public String getNickname(){
        return nickname;
    }
    public void setNickname(String nickname){
        this.nickname = nickname;
    }

    public Species getSpecies(){
        return species;
    }
    public void setSpecies(Species species){
        this.species = species;
    }

    public int getAge(){
        return age;
    }
    public void setAge(int age){
        this.age = age;
    }

    public int getTrickLevel(){
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel){
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits(){
        return habits;
    }
    public void setHabits(String habits){
        this.habits.add(habits);
    }


    public void eat(){
        System.out.println("Я кушаю!");
    }

    public abstract void respond();


    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + (habits==null ? "" : habits) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.printf("%s { nickname= %s , age= %d, trickLevel= %d, habits= %s }",species, nickname, age, trickLevel, (habits==null ? "" : habits) );
    }
}
