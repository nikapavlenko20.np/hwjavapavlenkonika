package hw4;

import java.util.Map;

public final class Man extends Human{

    public Man(String name, String surname, int iq, Map<DayOfWeek,String> schedule) {
        super(name, surname,iq, schedule);
    }

    public Man(String name,String surname){
        super(name, surname);
    }

    public Man(String name,String surname, String birthDate, int iq){
        super(name, surname, birthDate, iq);
    }

    public Man(String name,String surname, Family family){
        super(name, surname, family);
    }

    public Man(String name,String surname, int iq, Map<DayOfWeek,String> schedule, Family family){
        super(name, surname,iq, schedule, family);
    }

    public Man(){}

    public void greetPet(){
        getFamily().getPet().forEach(x -> System.out.printf("Привет, %s\n", x.getNickname()));
    }

    public void repairCar(){
        System.out.println("Моя прекрасная машина");
    }

}
