package hw4;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    @Test
    public void toStringExpected1( ){
        String expect = "Family{mother=Human{name='Sara', surname='Black', year=3, iq=12, schedule={THURSDAY=coking, FRIDAY=go to shop}}, father=Human{name='Sara', surname='Black', year=3, iq=12, schedule={THURSDAY=coking, FRIDAY=go to shop}}, children=[], pet=no pet}";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Woman mother = new Woman("Sara", "Black", 3, schedule);
        Man father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);
        String real = family.toString();
        assertEquals(expect,real);
    }


    @Test
    public void toStringExpected2( ){
        String expect = "Family{mother=Human{name='Sara', surname='Black', year=3, iq=12, schedule={THURSDAY=coking, FRIDAY=go to shop}}, father=Human{name='Sara', surname='Black', year=3, iq=12, schedule={THURSDAY=coking, FRIDAY=go to shop}}, children=[], pet=[RoboCat{nickname='mary', age=0, trickLevel=0, habits=}]}";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3, schedule);
        Human father = new Woman("Sara", "Black", 3, schedule);
        Pet dog = new RoboCat("mary");
        Family family = new Family(mother, father, dog);
        String real = family.toString();
        assertEquals(expect,real);
    }


    @Test
    public void toStringTest4(){
        String expect = "Human{name='Sara', surname='Black', year=3, iq=2, schedule={THURSDAY=coking, FRIDAY=go to shop}}";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3,  schedule);
        Human father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);

        Human child = new Woman("Sara", "Black", 3,schedule,family);
        String real = child.toString();
        assertEquals(expect,real);
    }

    @Test
    public void  deleteChildTest1(){
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3, schedule);
        Human father = new Man("Sara", "Black", 3,  schedule);
        Family family = new Family(mother, father);
        Human child = new Woman("Sara", "Black", 3, schedule,family);
        family.addChild(child);
        assertFalse(family.deleteChild(child));
    }


    @Test
    public void  deleteChildTest2(){
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };

        Human mother = new Woman("Sara", "Black", 3,  schedule);
        Human father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);
        Human child1 = new Woman("Sara", "Black", 3, schedule);
        Human child2 = new Woman("Saaaara", "Black", 3, schedule);
        family.addChild(child1);
        assertFalse(family.deleteChild(child2));
    }

    @Test
    public void  addChildTest(){
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3, schedule);
        Human father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);
        Human child = new Woman("Sara", "Black", 3, schedule);
        assertEquals(family.getChildren().size(), 0);
        family.addChild(child);
        assertEquals(family.getChildren().size(), 1);

        assertTrue(family.getChildren().contains(child));
    }

    @Test
    public void  countFamilyTest(){
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3,  schedule);
        Human father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);
        Human child = new Woman("Sara", "Black", "12/11/2020",3);
        family.addChild(child);
        assertEquals(family.countFamily(), 3);
    }
}
