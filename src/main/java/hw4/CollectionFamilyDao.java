package hw4;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private final ArrayList<Family> familyArrayList  = new ArrayList<>();


    public CollectionFamilyDao() {
    }



    @Override
    public ArrayList<Family> getAllFamilies() {
        return familyArrayList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
       return familyArrayList.get(index);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        int familyArrayListSize = familyArrayList.size();
        try {
            familyArrayList.remove(index);
            return familyArrayList.size() != familyArrayListSize;
        } catch (Exception x){
            return false;
        }
    }

    @Override
    public boolean deleteFamilyByFamily(Family family) {
        if (familyArrayList.contains(family)) {
            familyArrayList.remove(family);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {
        if (familyArrayList.contains(family)) {
            int indexFamily = familyArrayList.indexOf(family);
            familyArrayList.set(indexFamily, family);
        } else familyArrayList.add(family);
    }

    @Override
    public void loadData(List<Family> families) {
    }
}
