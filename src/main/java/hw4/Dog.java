package hw4;

import java.util.Set;

public class Dog extends Pet implements Foul{
    private Species species = Species.Dog;

    public Dog(String nickname){
        super(nickname);
        setSpecies(species);
    }

    public Dog( String nickname, int age, int trickLevel, Set<String> habits){
        super (nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    public Dog(){
        setSpecies(species);
    }

    @Override
    public void foul() {
        System.out.println(" Не нужно хорошо замести следы...");
    }

    @Override
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", getNickname());
    }
}
