package hw4;

public enum DayOfWeek {
    MONDAY("MONDAY"), TUESDAY("TUESDAY"), WEDNESDAY("WEDNESDAY"),
    THURSDAY("THURSDAY"), FRIDAY("FRIDAY"), SATURDAY("SATURDAY"),
    SUNDAY("SUNDAY");

    private String dayOfWeek;

    DayOfWeek(String day){
        this.dayOfWeek = day;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }
}
