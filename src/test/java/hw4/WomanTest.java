package hw4;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WomanTest {

    @Test
    public void toStringTest1(){
        String expect = "Привет, Sara";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human human1 = new Woman("Sara", "Black", 3, schedule);
        Human human2 = new Man("Sara", "Black", 3, schedule);
        Dog dog = new Dog("Sara");
        Family family = new Family(human1, human1, dog);
    }
}
