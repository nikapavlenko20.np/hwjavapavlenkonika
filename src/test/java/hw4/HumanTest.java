package hw4;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class HumanTest {

    @Test
   public void toStringTest1(){
        String expect = "Human{name='Sara', surname='Black', year=3, iq=0, schedule=}";
        Woman human = new Woman("Sara", "Black");
        String real = human.toString();
        assertEquals(expect,real);
    }

    @Test
    public void toStringTest2(){
        String expect = "Human{name='Sara', surname='Black', year=3, iq=12, schedule={THURSDAY=coking, FRIDAY=go to shop}}";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };        Human human = new Man("Sara", "Black", 3, schedule);
        String real = human.toString();
        assertEquals(expect,real);
    }

    @Test
    public void toStringTest3(){
        String expect = "Human{name='Sara', surname='Black', year=3, iq=0, schedule=}";
        Human woman = new Woman("Sara", "Black");
        String real = woman.toString();
        assertEquals(expect,real);
    }


    @Test
    public void toStringTest4(){
        String expect = "Human{name='Sara', surname='Black', year=3, iq=2, schedule={THURSDAY=coking, FRIDAY=go to shop}}";
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };          Human human1 = new Woman("Sara", "Black", 3, schedule);
        Human human2 = new Man("Sara", "Black", 3,  schedule);
        Family family = new Family(human1, human2);

        Human human3 = new Woman("Sara", "Black", 3, schedule,family);
        String real = human3.toString();
        assertEquals(expect,real);
    }




}
