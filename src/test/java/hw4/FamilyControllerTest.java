package hw4;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyControllerTest {
    @Test
    public void createNewFamilyTest(){
        FamilyService familyService = new FamilyService();
        Human mother1 = new Woman("Sara", "Black");
        Human father1 = new Man("Bill", "Black");
        Human child = new Man("Bill", "Black");
        Pet dog = new Dog( "Mary");

        FamilyController.familyService.createNewFamily(mother1, father1);
        familyService.displayAllFamilies();
        familyService.addPet(0,dog);

    }
}
