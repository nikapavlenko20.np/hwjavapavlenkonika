package hw4;

import java.util.Set;

public class DomesticCat extends Pet implements Foul{
    private Species species = Species.DomesticCat;


    public DomesticCat(String nickname){
        super(nickname);
        setSpecies(species);
    }

    public DomesticCat( String nickname, int age, int trickLevel,  Set<String> habits){
        super (nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    public DomesticCat(){
        setSpecies(species);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", getNickname());
    }
}
