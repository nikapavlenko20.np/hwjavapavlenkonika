package hw4;

import java.io.*;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyService {
    private final static FamilyDao familyDao = new CollectionFamilyDao();

    public FamilyService(){}

    public static FamilyDao getFamily(){
        return familyDao;
    }

    public void displayAllFamilies(){
        familyDao.getAllFamilies().forEach((x)-> {
            int i = 1;
            System.out.printf("%d. %s", i,x.prettyFormat());
        });
    }

    public  List<Family> getFamiliesBiggerThan(int a){
        List<Family> collect = familyDao.getAllFamilies().stream().filter(x -> x.countFamily() > a).collect(Collectors.toList());
        collect.forEach(x -> System.out.println(x.prettyFormat()));
        return collect;
    }

    public List<Family> getFamiliesLessThan(int a){
        List<Family> collect = familyDao.getAllFamilies().stream().filter(x -> x.countFamily() < a).collect(Collectors.toList());
        collect.forEach(x -> System.out.println(x.prettyFormat()));
        return collect;
    }

    public List<Family> getFamiliesInCount(int a){
        List<Family> collect = familyDao.getAllFamilies().stream().filter(x -> x.countFamily() == a).collect(Collectors.toList());
        return collect;
    }

    public int countFamiliesWithMemberNumber(int a){
        List<Family> collect = familyDao.getAllFamilies().stream().filter(x -> x.countFamily() > a).collect(Collectors.toList());
        return collect.size();
    }

    public void createNewFamily(Human mother, Human father){
       Family family =  new Family(mother,father);
       familyDao.saveFamily(family);
    }

    public void bornChild(Family family, Human child){
        family.addChild(child);
        familyDao.saveFamily(family);
    }

    public Family adoptChild(Family family, Human child){
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    static void readFile(List<Family> families, File file) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        try(ObjectInputStream oip = new ObjectInputStream(fileInputStream)){
            int elementCount = oip.readInt();
            System.out.println(elementCount);
            for (int i = 0; i < elementCount; i++) {
                Family family = (Family) oip.readObject();
                System.out.println(family);
                families.add(family);
            }
//            while (true){

//            }
        }catch (EOFException e){
            e.printStackTrace();
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void saveFile(List<Family> families, File file) throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream(file);
        try(ObjectOutputStream oop = new ObjectOutputStream(fos)){
            oop.writeInt(families.size());
            for (Family family: families) {
                System.out.println(families);
                oop.writeObject(family);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static public void loadData(List<Family> families) {
        File familyFile = new File("family.bin");
        System.out.println(families);
        try {
            if (familyFile.exists()) {
                readFile(families, familyFile);
            } else {
                saveFile(families, familyFile);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllChildrenOlderThen(int a){
        familyDao.getAllFamilies().stream().
                map(x -> {
                    ArrayList<Human> collect = (ArrayList<Human>) x.getChildren().stream()
                            .filter(y -> y.getOld() > a).collect(Collectors.toList());
                    x.setChildren(collect);
                    return null;
                });
    }


    public Set<Pet> getPets(int index){
        return familyDao.getFamilyByIndex(index).getPet();
    }

    public void addPet(int indexFamily, Pet pet){
        Family family = familyDao.getFamilyByIndex(indexFamily);
        Set<Pet> pets = family.getPet();
        pets.add(pet);
        family.setPet(pets);
        familyDao.saveFamily(family);
    }
}
