package hw4;

import java.util.ArrayList;
import java.util.List;

public interface FamilyDao {
   public ArrayList<Family> getAllFamilies();
   public Family getFamilyByIndex(int index);
   public boolean deleteFamilyByIndex(int index);
   public boolean deleteFamilyByFamily(Family family);
   public void saveFamily(Family family);
   public void loadData(List<Family> families);
}
