package hw4;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class Family implements java.io.Serializable {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private Set<Pet> pet = new HashSet<Pet>();

    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children = new ArrayList<Human>();
    }

    public Family(Human mother, Human father, Pet pet){
        this.mother = mother;
        this.father = father;
        this.pet.add(pet);
        this.children = new ArrayList<Human>();
    }

    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public ArrayList<Human>getChildren() {
        return children;
    }
    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }
    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public void addChild(Human child){
        this.children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(Human child){
        int sizeStart = this.children.size();
        this.children.removeIf(c -> c.equals(child));

        return sizeStart == this.children.size();
    };

    public int countFamily(){
        return children.size() + 2;
    }

    public String prettyFormat(){
        List<String> childrenString = this.children.stream().map(x -> {
            if (x.getClass() == Woman.class) return format("\n %13s girl: %s \n", " ", x.toString());
            if (x.getClass() == Man.class) return format("\n %13s boy: %s \n", " ", x.toString());
            return "";
        }).collect(Collectors.toList());

        return format("family: \n %5s mother: %s \n %5s father: %s \n %5s children: %s \n %5s pets: %s", "",this.mother, "", this.father, "",  childrenString, "", this.pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString()+
                ", children=" + children +
                ", pet=" +( pet==null ? "no pet" : pet.toString()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children);
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.printf("Family{ mother %s, father= %s, children= %s, pet= %s", mother.toString(),father.toString(), children, pet);
    }
}
