package hw4;

import java.util.Set;

public class RoboCat  extends Pet implements Foul{
    private Species species = Species.RoboCat;

    public RoboCat(String nickname){
        super(nickname);
        setSpecies(species);
    }

    public RoboCat( String nickname, int age, int trickLevel,  Set<String> habits){
        super (nickname, age, trickLevel, habits);
        setSpecies(species);
    }

    public RoboCat(){
        setSpecies(species);
    }

    @Override
    public void foul() {
        System.out.println("Все равно уберут...");
    }

    @Override
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", getNickname());
    }
}
