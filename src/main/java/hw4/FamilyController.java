package hw4;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class FamilyController {
    public static FamilyService familyService = new FamilyService();

    public FamilyController(){
        FamilyService.getFamily().saveFamily(createFamily());
    }


    public static Family createFamily(){
        Map<DayOfWeek, String> schedule = new HashMap<DayOfWeek, String>(){
            {
                put(DayOfWeek.THURSDAY, "coking");
                put(DayOfWeek.FRIDAY, "go to shop");
            }

        };
        Human mother = new Woman("Sara", "Black", 3, schedule);
        Human father = new Man("Sara", "Black", 3, schedule);
        Family family = new Family(mother, father);
        Human child = new Woman("Sara", "Black", 3, schedule);
        family.addChild(child);
        return family;
    }

    public void getFamiliesBiggerThan(int a){
        familyService.getFamiliesLessThan(a);
    }

    public void getFamiliesLessThan(int a){
        familyService.getFamiliesLessThan(a);
    }

    public int count(){
        return FamilyService.getFamily().getAllFamilies().size();
    }

    public int getFamiliesInCount(int a){
        return familyService.getFamiliesInCount(a).size();
    }

    public void deleteAllChildrenOlderThen(int a){
        familyService.deleteAllChildrenOlderThen(a);
    }


    public void bornChild(int numberFamily, String name){
        try {
            Family family = FamilyService.getFamily().getFamilyByIndex(numberFamily);
            Human child = new Man();
            if (name == "Nika") child = new Woman(name, family.getFather().getSurname(), family);
            if (name == "Alex") child = new Man(name, family.getFather().getSurname(), family);
            familyService.bornChild(family, child);
        } catch (FamilyOverflowException x ){
            System.out.println(x.toString());
        }
    }

    public void loadData() {
        try{
            familyService.loadData(FamilyService.getFamily().getAllFamilies());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void adoptChild(int numberFamily,Human child ){
        try {
            Family family = FamilyService.getFamily().getFamilyByIndex(numberFamily);
            familyService.adoptChild(family, child);
        } catch (FamilyOverflowException x ){
            System.out.println(x.toString());
        }

    }

}
